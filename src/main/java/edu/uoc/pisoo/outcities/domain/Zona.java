package edu.uoc.pisoo.outcities.domain;

public class Zona {

    private final Long id;

    public Zona(Long id){
        this.id = id;
    }

    public Long getId() {
        return id;
    }
}
