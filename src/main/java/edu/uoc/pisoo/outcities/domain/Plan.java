package edu.uoc.pisoo.outcities.domain;

import java.util.LinkedHashSet;

public class Plan {
    private final Long id;
    private int numVisitas=0;
    private final LinkedHashSet<Opinion> opiniones = new LinkedHashSet<Opinion>();
    private final LinkedHashSet<Usuario> fans = new LinkedHashSet<Usuario>();

    public Plan(Long id) {
        this.id = id;
    }

    void addOpinion(Opinion opinion) {
        this.opiniones.add(opinion);
    }

    public void nuevaVisita(){
        numVisitas++;
    }

    // Getters generados


    public Long getId() {
        return id;
    }

    public LinkedHashSet<Opinion> getOpiniones() {
        return opiniones;
    }

    public LinkedHashSet<Usuario> getFans() {
        return fans;
    }

    public int getNumVisitas() {
        return numVisitas;
    }
}
